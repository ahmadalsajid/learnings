# List

## Built-ins:
- list.append(x)
- list.extend(iterable)
- list.insert(i, x)
- list.remove(x)
  - Remove the first item from the list whose value is equal to x.
- list.pop([i])
- list.clear()
  - Remove all items from the list. Equivalent to del a[:].
- list.index(x[, start[, end]])
- list.count(x)
- list.sort(key=None, reverse=False)
- list.reverse()
- list.copy()

## Examples:
```
>>> digits = [1, 2, 3, 4, 5]
>>> digits
[1, 2, 3, 4, 5]
>>> digits.append(6)
>>> digits
[1, 2, 3, 4, 5, 6]
>>> digits.extend([7, 8, 9])
>>> digits
[1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> digits.insert(0, 0)
>>> digits
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> digits.insert(0, 0)
>>> digits
[0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> digits.remove(0)
>>> digits
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> digits.insert(5, 0)
>>> digits.insert(8, 2)
>>> digits
[0, 1, 2, 3, 4, 0, 5, 6, 2, 7, 8, 9]
>>> digits.append(9)
>>> digits.index(0)
0
>>> digits.index(0,1)
5
>>> digits.index(0, 1, 3)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: 0 is not in list
>>> digits.count(9)
2
>>> digits.sort()
>>> digits
[0, 0, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9, 9]
>>> digits.sort(reverse=True)
>>> digits
[9, 9, 8, 7, 6, 5, 4, 3, 2, 2, 1, 0, 0]
>>> digits.reverse()
>>> digits
[0, 0, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9, 9]
>>> digits.pop()
9
>>> digits.pop(0)
0
>>> digits.insert(0, 10)
>>> digits
[10, 0, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9]
>>> digits.reverse()
>>> digits
[9, 8, 7, 6, 5, 4, 3, 2, 2, 1, 0, 10]
>>> d = digits.copy()
>>> d
[9, 8, 7, 6, 5, 4, 3, 2, 2, 1, 0, 10]
>>> d.reverse()
>>> d
[10, 0, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9]
>>> digits
[9, 8, 7, 6, 5, 4, 3, 2, 2, 1, 0, 10]
>>> d.clear()
>>> d
[]
```
## List Comprehensions
```
>>> squares = []
>>> for x in range(10):
...     squares.append(x**2)
...
>>> squares
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
```
same as

```
squares = list(map(lambda x: x**2, range(10)))
```
or
```
squares = [x**2 for x in range(10)]
```

## Nested List Comprehensions
```
>>> matrix = [
...     [1, 2, 3, 4],
...     [5, 6, 7, 8],
...     [9, 10, 11, 12],
... ]
```
to transpose
```
>>> [[row[i] for row in matrix] for i in range(4)]
[[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]
```
pythonic way
```
>>> list(zip(*matrix))
[(1, 5, 9), (2, 6, 10), (3, 7, 11), (4, 8, 12)]
```
