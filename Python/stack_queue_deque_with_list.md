# Stack with List
```
class Stack():
    def __init__(self, stack=[]):
        self.stack = stack

    def push(self, item):
        self.stack.append(item)

    def pop(self):
        try:
            return self.stack.pop()
        except Exception:
            return None

    def peek(self):
        return self.stack[-1] if self.stack else None

    def display(self):
        return self.stack


if __name__ == '__main__':
    s = Stack()
    print(s.peek())
    s.push(1)
    s.push(2)
    print(s.display())
    print(s.peek())
    s.push(5)
    print(s.pop())
    s.push(3)
    print(s.peek())
    print(s.pop())
    print(s.pop())
    print(s.pop())
    print(s.pop())
    s.push(4)
    print(s.display())
```


# Queue with list
```
class Queue():
    def __init__(self, l=[]):
        self.queue = l

    def display(self):
        return self.queue

    def get_front(self):
        return self.queue[0] if self.queue else None

    def enqueue(self, item):
        self.queue.append(item)

    def dequeue(self):
        return self.queue.pop(0) if self.queue else None


if __name__ == '__main__':
    q = Queue()
    q.enqueue(1)
    q.enqueue(2)
    print(q.display())
    print(q.dequeue())
    print(q.dequeue())
    print(q.dequeue())
    q.enqueue(1)
    q.enqueue(2)
    print(q.get_front())

```

# Dequeue with List
```
class Dequeue():
    def __init__(self, l=[]):
        self.dq = l

    def get_display(self):
        return self.dq

    def insert_front(self, item):
        self.dq.insert(0, item)

    def insert_last(self, item):
        self.dq.append(item)

    def delete_front(self):
        return self.dq.pop(0) if self.dq else None

    def delete_last(self):
        return self.dq.pop() if self.dq else None

    def get_front(self):
        return self.dq[0] if self.dq else None

    def get_last(self):
        return self.dq[-1] if self.dq else None


if __name__ == '__main__':
    dq = Dequeue([1,2,3])
    print(dq.get_display())
    dq.insert_front(10)
    dq.insert_last(15)
    print(dq.get_last())
    print(dq.get_front())
    print(dq.get_display())
    print(dq.delete_front())
    print(dq.delete_last())
    print(dq.get_display())
```
