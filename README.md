# Learnings

This project is a collection of the learnings on various topics from various
resources that would be described as what I have understood. Basically, here,
I will keep them as markdown files in category wise folders. For coding or other
references, I will use other projects or gists depending on the situations.

## Learning wish list
- [Python][pthn]
    - Data types
    - Data Structures
      - [List](Python/list.md)
      - [Stack / Queue / Deque with List](Python/stack_queue_deque_with_list.md)
      - [Collections](Python/collections.md)
- [Django][djngo]
- [Django Rest Framework][drf]
- Unit Testing
- Celery / RabbitMQ
- Memcached
- Gunicorn
- Nginx
- Git
- CI/CD
- Jenkins
- Database
- React.js


[pthn]: <https://www.python.org/>
[djngo]: <https://www.djangoproject.com/>
[drf]: <https://www.django-rest-framework.org/>
